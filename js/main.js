function setup()
{
	var win = createCanvas(700, 700);
	win.id("win")

/*
	var inputPointLevel = document.getElementById('inputPointLevel')
	inputPointLevel.onchange = function()
	{
		pointLevel = inputPointLevel.value.split(" ");
		console.log(pointLevel);
		plot1.pointLineLevel = inputPointLevel.value.split(" ");
		plot1.drawIntervalLine();
	};

	var inputColor = document.getElementById('inputColor')
	inputColor.onchange = function()
	{
		var temp = inputColor.value.split(" ");
		var temp1 = [];
		for (var i = 0; i < (temp.length - temp.length % 3)/3 ; i++)
			temp1[i] = color(temp[i*3], temp[i*3+1], temp[i*3+2]);
		plot1.pointColorIntervalLine = temp1;
		console.log(temp);
		console.log(temp1);
	};

	var pointLevel = [-.9, -.7, -.5, -.3, -.1, 0.01, .1, .3, .5, .7, .9, 1.2, 2.2, 2.6];
	var colorInterval = [];
	for (var i = 0; i < pointLevel.length-1; i++)
		colorInterval[i] = color(0,0, 255/(pointLevel.length-1) * i)

	var f = function(x,y)
	{
		return (x*x+2*pow( 3./5*pow(x*x,1./3)-y, 2 )-1);
		//return x*x + y*y;
	}

plot1 = new drawPlot(-1, 1, -1, 1, f, pointLevel, colorInterval);
//plot1.drawLineLevel(); //plot1.clrDraw2D = color(223,123,214);
//plot1.draw2D();
plot1.axisCoordinates(0, 0, 0.1, 0.1);
*/
};

//==============================================================================

var XYR = function(x, y, d)
{
	this.x = x;
	this.y = y;
	this.r = d/2;
	this.d = d;
}
var ellipseXYR = [new XYR(20, 50, 20), new XYR(40, 200, 20)];
var actionEllipse = false;
var indexAction;

function draw()
{
	background(color(100,100,100));

	for (var i = 0; i < ellipseXYR.length; i++)
	{
		ellipse(ellipseXYR[i].x, ellipseXYR[i].y, ellipseXYR[i].d);
	}

	if (mouseIsPressed)
	{
		if (!actionEllipse)
		{
			for (var i = 0; i < ellipseXYR.length; i++)
			{
				if ( pow(mouseX - ellipseXYR[i].x, 2) + pow(mouseY - ellipseXYR[i].y, 2) <= pow(ellipseXYR[i].r, 2) )
				{
					actionEllipse = true;
					indexAction = i;
					break;
				}
			}
		}
		else
		{
			ellipseXYR[indexAction].x = mouseX;
			ellipseXYR[indexAction].y = mouseY;
		}
	}

	var nodeX = [];
	var nodeY = [];
	for (var i = 0; i < ellipseXYR.length; i++)
	{
		nodeX[i] = ellipseXYR[i].x;
		nodeY[i] = ellipseXYR[i].y;
	}

	//console.log(nodeX);
	//console.log(nodeY);
	//var intropolation = new intropolation2D([0,2, 3, 4, 5,6], [0,4,1,3,2,5]);
	var intropolation = new intropolation2D(nodeX, nodeY);
	intropolation.draw();

	//console.log("mouseX" ,mouseX, " mouseY", mouseY);
};

//==============================================================================

function mouseReleased()
{
	actionEllipse = false;
}

//==============================================================================

function mousePressed()
{
	var bool = true;
	for (var i = 0; i < ellipseXYR.length; i++)
	{
		if ( pow(mouseX - ellipseXYR[i].x, 2) + pow(mouseY - ellipseXYR[i].y, 2) <= pow(ellipseXYR[i].r*2, 2) )
		{
			bool = false;
			break;
		}
	}
	if (bool)
	{
		var d = 20;
		ellipseXYR.push(new XYR(mouseX, mouseY, d));
	}
}

//==============================================================================

var intropolation2D = function(nodeX, nodeY, mathOrGraph, a, b, c, d)
{
	this.locationFun = [];
	this.nodeX = nodeX;
	this.nodeY = nodeY;

	this.minMax = function(minMax, nY)
	{
		if (nY == true)
		{
			var min = minMax[0];
			for (var i = 1; i < minMax.length; i++)
			{
				if (minMax[i] < min)
					min = minMax[i];
			}
			return min;
		}
		else
		{
			var max = minMax[0];
			for (var i = 1; i < minMax.length; i++)
			{
				if (minMax[i] > max)
					max = minMax[i];
			}
			return max;
		}
	}

	this.a = a || mathOrGraph?this.minMax(this.nodeX, true): 0;
	this.b = b || mathOrGraph?this.minMax(this.nodeX, false): width;
	this.c = c || mathOrGraph?this.minMax(this.nodeY, true): 0;
	this.d = d || mathOrGraph?this.minMax(this.nodeY, false): width;

	this.fCub = function(a, b, c, d, xi)
	{
		this.a = a;
		this.b = b;
		this.c = c;
		this.d = d;
		this.xi = xi;

		this.f = function(x)
		{
			return a*pow(x - xi, 3) + b*pow(x - xi, 2) + c*(x - xi) + d;
		}
	}

	this.searchCoefficent = function()
	{
		var df = [];
		if (this.nodeX.length > 1)
		{
			var deltaY = this.nodeY[1] - this.nodeY[0];
			var deltaX = this.nodeX[1] - this.nodeX[0];

			df[0] = deltaY / deltaX;

			var deltaX = this.nodeX[nodeX.length-1] - this.nodeX[nodeX.length-2];
			var deltaY = this.nodeY[nodeX.length-1] - this.nodeY[nodeX.length-2];

			df[nodeX.length-1] = deltaY / deltaX;
		}

		for (var i = 1; i < this.nodeX.length-1; i++)
		{
			var f0 = this.nodeY[i-1];
			var f1 = this.nodeY[i];
			var f2 = this.nodeY[i+1];

			if ( (f0 > f1 && f1 < f2) || (f0 < f1 && f1 > f2) )
			{
				df[i] = 0;
			}
			else
			{
				var deltaX = this.nodeX[i+1] - this.nodeX[i-1];
				var deltaY = f2 - f0;

				df[i] = deltaY / deltaX;
			}
		}

		for (var i = 0; i < this.nodeX.length-1; i++)
		{
			var h = this.nodeX[i+1] - this.nodeX[i];

			var f = this.nodeY[i];
			var f1 = this.nodeY[i+1];

			var df0 = df[i];
			var df1 = df[i+1];

			var a1 = pow(h,3);
			var a2 = pow(h, 2);
			var a3 = 3*a2;
			var a4 = 2*h;
			var b1 = f1 - df0*h - f;
			var b2 = df1 - df0;

			var bi = (b2*a1 - b1*a3) / (a4*a1 - a3*a2);
			var ai = (b1 - a2*bi) / a1;
			var ci = df0;
			var di = nodeY[i];
			var xi = nodeX[i];

			this.locationFun[i] = new this.fCub(ai, bi, ci, di, xi);
		}
		return this.locationFun;
	}

	this.mappingX = function(x)
	{
		return (x - this.a) / (this.b - this.a) * width;
	}

	this.mappingY = function(y)
	{
		return height - (y - this.c) / (this.d - this.c) * height;
	}

	this.draw = function()
	{
		var arr = this.searchCoefficent();

		//console.log("a = ", this.a, " b = ", this.b, " c = ", this.c," d = ", this.d);

		for (var i = 0; i < this.nodeX.length; i++)
		{
			var x = (this.nodeX[i] - this.a)/(b - this.a)*width;
			var y = (this.nodeY[i] - this.c) / (this.d - this.c)*height;
			fill(0);
			ellipse(x, height-y, 10, 10);
		}

		var delPlot = 100;

		for (var i = 0; i < arr.length; i++)
		{
			var x = this.nodeX[i];
			var xStep = (this.nodeX[i+1] - this.nodeX[i]) / delPlot;

			//console.log("x = ", x);
			//console.log("xStep = ", xStep);

			var xLast = mathOrGraph?mappingX(x): x;
			var yLast = mathOrGraph?mappingY(arr[i].f(x)): arr[i].f(x);

			//console.log(arr);
			//console.log("xLast = ", xLast);
			//console.log("yLast = ", yLast);

			var xNow, yNow;

			for (var j = 0; j < delPlot; j++)
			{
				stroke(color(0,0,255));
				x += xStep;
				xNow = mathOrGraph?mappingX(x): x;
				yNow = mathOrGraph?mappingY(arr[i].f(x)): arr[i].f(x);
				line(xLast, yLast, xNow, yNow);
				xLast = xNow;
				yLast = yNow;
				//console.log("xNow = ", xNow);
				//console.log("yNow = ", yNow);
			}
		}
	}
};

//==============================================================================

var drawPlot = function(a, b, c, d, f, pointArray, colorArray )
{
	this.pointLineLevel = pointArray || [-1, 0, 1];

	this.pointColorIntervalLine = colorArray || [color(0,0,255), color(0,0,150)];

	this.a = a;
	this.b = b;
	this.c = c;
	this.d = d;

	this.xStep = (b - a) / width;
	this.yStep = (d - c) / height;

	this.f = f;
	this.clrDraw2D = color(0,0,0)
	this.clrAxis = color(0,0,0);

	this.drawIntervalLine = function()
	{
		var that = this;
		var checkColorPixel3D = function(z)
		{
			for (var i = 0; i < that.pointLineLevel.length-1; i++)
			{
				if ( that.pointLineLevel[i] < z && z < that.pointLineLevel[i+1] )
				{
					return that.pointColorIntervalLine[i];
				}
			}
		}
		var x = this.a;
		var y = this.c;

		for (var i = height; i--;)
		{
			y += this.yStep;
			x = this.a;
			for (var j = 0; j < width; j++)
			{
				x += this.xStep;

				if ( checkColorPixel3D( this.f(x, y) ) )
				{
					stroke( checkColorPixel3D( this.f(x, y) ) );
					point(j, i);
				}
			}
		}
	}

	this.drawLineLevel = function()
	{
		var x = this.a;
		var y = this.c;

		for (var i = height-1; i--;)
		{
			y += this.yStep;
			x = this.a;
			for (var j = 0; j < width-1; j++)
			{
				x += this.xStep;

				var z = this.checkColorPixel3D( this.f(x, y) );
				var zx = this.checkColorPixel3D( this.f(x+this.xStep, y) );
				var zy = this.checkColorPixel3D( this.f(x, y+this.yStep) );

				if (z !== zx || z !== zy)
				{
					stroke(color(0,0,0));
					point(j, i);
				}
			}
		}
	}
	this.mappingX = function(x)
	{
		return (x - this.a) / (this.b - this.a) * width;
	}

	this.mappingY = function(y)
	{
		return height - (y - this.c) / (this.d - this.c) * height;
	}

	this.axisCoordinates = function(axisX, axisY, delX, delY)
	{
		stroke(this.clrAxis);
		//Oy
		line(this.mappingX(axisY), 0, this.mappingX(axisY), height);
		//Ox
		line(0, this.mappingY(axisX), width, this.mappingY(axisX));

		var xCountLine = (this.b - this.a) / delX;
		var yCountLine = (this.d - this.c) / delY;

		var longLine = 5;

		//Штрихы по Ox
		var x = this.a;
		for (var i = 0; i < xCountLine; i++)
		{
			x += delX;
			line(this.mappingX(x), this.mappingY(axisX) - longLine, this.mappingX(x), this.mappingY(axisX) + longLine);
		}

		//Штрихы по Oy
		var y = this.c;
		for (var i = 0; i < yCountLine; i++)
		{
			y += delY;
			line(this.mappingX(axisY) - longLine, this.mappingY(y), this.mappingX(axisY) + longLine, this.mappingY(y));
		}
	}

	this.draw2D = function()
	{
		stroke(this.clrDraw2D);
		var delPlot = 100;
		var xStep = (this.b - this.a) / delPlot;

		var x = this.a;
		var xLast = this.mappingX(x), yLast = this.mappingY(this.f(x));
		for (var i = 0; i < delPlot; i++)
		{
			x += xStep;
			var xNow = this.mappingX(x), yNow = this.mappingY(this.f(x));
			line(xLast, yLast, xNow, yNow);
			xLast = xNow;
			yLast = yNow;
		}
	}
};